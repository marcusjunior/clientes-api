package com.clientes.clientes.application.command;

import com.clientes.clientes.domain.model.cliente.Cliente;
import com.clientes.clientes.domain.model.cliente.CriarCliente;

public class ClienteCommandsHandlerMockFactory {

    public static Cliente cliente() {
        return Cliente
                .builder()
                .id(1L)
                .nome("Cliente 1")
                .numeroConta(1L)
                .saldo(800D)
                .build();
    }

    public static CriarCliente criarCliente() {
        return CriarCliente
                .builder()
                .id(1L)
                .nome("Cliente 1")
                .numeroConta(1L)
                .saldo(800D)
                .build();
    }
}
